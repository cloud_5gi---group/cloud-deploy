if ! [ -x "$(command -v figlet)" ]; then  
    sudo apt install -y figlet
fi
if [ -x "$(command -v figlet)" ]; then  
    figlet Projet Cloud
fi

echo "---------------------------------------------------------"
echo "             Clone All Source Code Of Project            "
echo "---------------------------------------------------------"

git clone https://gitlab.com/cloud_5gi---group/cloud-frontend.git
git clone https://gitlab.com/cloud_5gi---group/blockchain-module.git
git clone https://gitlab.com/cloud_5gi---group/cloud-deploy.git &&
git clone https://gitlab.com/Venusdjinni/pollingrecordsparser.git
