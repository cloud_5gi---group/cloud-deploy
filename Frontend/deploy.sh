echo "----------------------------------------"
echo "     Deployment of Frontend API"
echo "----------------------------------------"

start_cloud-frontend(){
    echo "Starting Cloud-Frontend Container Listining on Port: $1"
    FRONTEND_PORT=$1 docker-compose up -d
    echo
    echo " Container Started"
    echo "Cloud-frontend running on http://localhost:$1"
}

stop_cloud-frontend(){
    echo "Stopping Cloud-Frontend Listening on Port: http://localhost:$1"
    FRONTEND_PORT=$1 docker-compose stop
    echo
    echo " Container Stopped"    
}

if [ -z $1 ] || [ -z $2 ]
then
    echo "Usage ./deploy.sh port(example: 3002) start/stop"
else
    case $2 in
    start)
        start_cloud-frontend $1
        ;;
    stop)
        stop_cloud-frontend $1
        ;;
    *)
    echo "Please add second parameter"
    esac
fi
