if ! [ -x "$(command -v figlet)" ]; then  
    sudo apt install -y figlet
fi
if [ -x "$(command -v figlet)" ]; then  
    figlet Projet Cloud
fi

# Exit on any failure
set -e

# Array of supported versions
declare -a versions=('trusty' 'xenial' 'yakkety', 'bionic');

# check the version and extract codename of ubuntu if release codename not provided by user
if [ -z "$1" ]; then
    source /etc/lsb-release || \
        (echo "Error: Release information not found, run script passing Ubuntu version codename as a parameter"; exit 1)
    CODENAME=${DISTRIB_CODENAME}
else
    CODENAME=${1}
fi
 
echo "----------------------------------------"
echo "  Installation of preres for the project"
echo "----------------------------------------"


# Install Git
if ! [ -x "$(command -v git)" ]; then 
    # Update package lists
    echo "1. Installing Git"
    echo "# Updating package lists"
    sudo apt-add-repository -y ppa:git-core/ppa
    sudo apt-get update

    sudo apt-get install -y git
    figlet Projet Cloud
fi


if ! [ -x "$(command -v docker)" ]; then 
    echo "2. Installation of Docker and Docker-Compose"
    #Install Docker and Docker-Compose
    # Ensure that CA certificates are installed
    sudo apt-get -y install apt-transport-https ca-certificates

    # Add Docker repository key to APT keychain
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    # Update where APT will search for Docker Packages
    echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${CODENAME} stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list

    # Update package lists
    sudo apt-get update

    # Verifies APT is pulling from the correct Repository
    sudo apt-cache policy docker-ce

    # Install kernel packages which allows us to use aufs storage driver if V14 (trusty/utopic)
    if [ "${CODENAME}" == "trusty" ]; then
        echo "# Installing required kernel packages"
        sudo apt-get -y install linux-image-extra-$(uname -r) linux-image-extra-virtual
    fi

    # Install Docker
    echo "# Installing Docker"
    sudo apt-get -y install docker-ce

    # Add user account to the docker group
    sudo usermod -aG docker $(whoami)

    # Install docker compose
    echo "# Installing Docker-Compose"
    sudo curl -L "https://github.com/docker/compose/releases/download/1.13.0/docker-compose-$(uname -s)-$(uname -m)" \
        -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi


if ! [ -x "$(command -v java)" ]; then 
  # Install Java
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt -y update
    sudo apt -y install oracle-java8-installer
    sudo apt install -y oracle-java8-set-default
fi

echo ''
echo 'Installation completed, versions installed are:'
echo ''
echo -n 'Git:           '
git --version
echo -n 'Docker:         '
docker --version
echo -n 'Docker Compose: '
docker-compose --version
echo -n 'Java:         '
java -version
