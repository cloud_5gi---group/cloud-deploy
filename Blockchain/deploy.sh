echo "----------------------------------------"
echo "     Deployment of Blockchain API"
echo "----------------------------------------"

start_blockchain(){
    echo "Starting Blockchain Container At Url:$1, Listining on Port: $2"
    BLOCKCHAIN_URL=$1 BLOCKCHAIN_PORT=$2 docker-compose up -d
    echo
    echo "Blockchain Container Started"
    echo "Blochchain running on $1"
}

stop_blockchain(){
    echo "Stopping Blokchain Container At Url:$1, Listening on Port: $2"
    BLOCKCHAIN_URL=$1 BLOCKCHAIN_PORT=$2 docker-compose stop
    echo
    echo "Blockchain Container Stopped"    
}

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]
then
    echo "Usage ./deploy.sh url(example: http://localhost:3002) port(example: 3002) start"
else
    case $3 in
    start)
        start_blockchain $1 $2
        ;;
    stop)
        stop_blockchain $1 $2
        ;;
    *)
    echo "Please add third parameter"
    esac
fi
